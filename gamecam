#!/usr/bin/env python3

import os
import time
import signal
import logging
import argparse
import tempfile
import subprocess

import cv2

logging.basicConfig(
    level='INFO')

##################################################

REFRESH_RATE = 1

class SSHFSTempDir(object):
    def __init__(self, remote):
        self.tdir = tempfile.TemporaryDirectory(prefix='gamecam_')
        subprocess.run(
            ['sshfs', remote, self.tdir.name],
            check=True,
        )

    def __enter__(self):
        return self.tdir.name

    def __exit__(self, type, value, traceback):
        logging.info("sshfs umount...")
        subprocess.run(
            ['fusermount', '-u', self.tdir.name],
        )
        logging.info("rmtdir...")
        self.tdir.cleanup()


def scp(path, remote):
    cmd = ['scp', '-C', path, remote]
    logging.info(cmd)
    subprocess.run(cmd)

##################################################

parser = argparse.ArgumentParser(
    prog='gamecam',
    description="""Take snapshot from webcam and upload to remote server via ssh.
""",
)
parser.add_argument(
    'device',
    help="camera device (e.g. '/dev/video4')")
parser.add_argument(
    'remote',
    help="remote image path as 'host:/webdir/path/gamecam.png''")
parser.add_argument(
    '--rate', '-r', default=REFRESH_RATE,
    help="refresh rate in seconds (default: {})".format(REFRESH_RATE))


def main():
    args = parser.parse_args()

    rotate = False
    sshfs = False

    imagename = os.path.basename(args.remote)

    if sshfs:
        def tempdir():
            return SSHFSTempDir(args.remote)
    else:
        def tempdir():
            return tempfile.TemporaryDirectory(prefix='gamecam_')


    with tempdir() as tdir:
        logging.info(tdir)

        ipath = os.path.join(tdir, imagename)
        itpath = ipath+'.tmp.png'

        cam = cv2.VideoCapture(args.device)

        while True:
            logging.info("capture {} ...".format(ipath))
            img = cam.read()[1]

            if rotate:
                print(img.shape)
                rows, cols, _ = img.shape
                M = cv2.getRotationMatrix2D((cols, rows), 90, 1)
                dst = cv2.warpAffine(img, M, (cols, rows))
            else:
                dst = img

            cv2.imwrite(itpath, dst)
            os.remove(ipath)
            os.rename(itpath, ipath)

            if not sshfs:
                scp(ipath, args.remote)

            time.sleep(args.rate)


if __name__ == '__main__':
    # signal.signal(signal.SIGINT, signal.SIG_DFL)
    try:
        main()
    except KeyboardInterrupt:
        raise SystemExit()
